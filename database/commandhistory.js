const commandHistory =
    [
        {
            id: 1,
            restaurant: "Gastro",
            product: "Frite",
            price: 3000,
            date: "08/03 à 16:05"
        },
        {
            id: 2,
            restaurant: "Extra",
            product: "Tacos",
            price: 10000,
            date: "08/03 à 18:08"
        },
        {
            id: 3,
            restaurant: "BurgerKing",
            product: "Burger",
            price: 9000,
            date: "08/03 à 20:15"
        }
    ]
export default commandHistory