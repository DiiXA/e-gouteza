import { View, Text, StyleSheet } from 'react-native'
import React from 'react'

const DisplaySalesHistory = (props) => {
  return (
    <View>
      <Text style={styles.Sales}> {props.product} {props.price} Ariary {props.date}</Text>
    </View>
  )
}

const styles = StyleSheet.create({
  Sales: {
    padding: 50,
    backgroundColor: "#dedede",
    margin: 5,
    borderRadius: 10
  }
})

export default DisplaySalesHistory