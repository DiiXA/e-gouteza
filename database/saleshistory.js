const salesHistory = { 
    Gastro : [
        {
            id: 1,
            restaurant: "Gastro",
            product: "Frite",
            price: 3000,
            date: "08/03 à 16:05"
        },
        {
            id: 2,
            restaurant: "Gastro",
            product: "Pizza",
            price: 20000,
            date: "09/03 à 18:08"
        },
        {
            id: 3,
            restaurant: "Gastro",
            product: "Sandwich Kebab",
            price: 9000,
            date: "15/03 à 20:15"
        }
    ],
    Extra : [
        {
            id: 1,
            restaurant: "Extra",
            product: "Tacos",
            price: 10000,
            date: "08/03 à 18:08"
        },
        {
            id: 2,
            restaurant: "Extra",
            product: "Rolls",
            price: 10000,
            date: "12/03 à 19:18"
        }
    ],
    KFC : [
        {
            id: 1,
            restaurant: "KFC",
            product: "Zinger",
            price: 17000,
            date: "05/03 à 10:08"
        }
    ],
    BurgerKing : [
        {
            id: 1,
            restaurant: "BurgerKing",
            product: "Triple Burger",
            price: 22000,
            date: "09/03 à 14:15"
        },
        {
            id: 2,
            restaurant: "BurgerKing",
            product: "Whopper",
            price: 17000,
            date: "12/03 à 17:18"
        }
    ],
  }
  
  export default salesHistory;