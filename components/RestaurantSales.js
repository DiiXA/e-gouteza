import { StyleSheet, TouchableOpacity, View, FlatList, Modal, Pressable, Text } from 'react-native'
import React, { useState } from 'react'
import DisplaySalesHistory from './DisplaySalesHistory';

const RestaurantSales = (props) => {
  const [modalVisible, setModalVisible] = useState(false);
  const renderItem = ({ item }) => (
    <DisplaySalesHistory restaurant={item.restaurant} place={item.place} product={item.product} price = {item.price} date = {item.date}/>
   );
  return (
    <View>
        <TouchableOpacity style={styles.listRestaurant} onPress={() => setModalVisible(true)}>
            <Text>{props.title}</Text>
        </TouchableOpacity>
        <Modal
            animationType="slide"
            transparent={false}
            visible={modalVisible}
            onRequestClose={() => {
            setModalVisible(!modalVisible);
            }}
        >
            <View style={styles.topView}>
                <View style={styles.modalView}>
                    <Pressable
                        style={[styles.button, styles.buttonClose]}
                        onPress={() => setModalVisible(!modalVisible)}
                        >
                        <Text style={styles.Return(props.colorscheme)}>←</Text>
                    </Pressable>
                    <FlatList
                        data={props.dbName}
                        renderItem={renderItem}
                        keyExtractor={item => item.id}
                        horizontal={false}
                    />
                </View>
            </View>
        </Modal>
    </View>
  )
}

export default RestaurantSales

const styles = StyleSheet.create({
    Return : (colorscheme) => { return {
        fontSize: 50,
        marginTop : -20,
        backgroundColor: colorscheme,
        color: "black"
    }},
    listRestaurant: {
        fontSize: 50,
        backgroundColor: "#b7ede8",
        color: "black",
        padding: 25,
        borderRadius: 25,
        marginVertical: 5,
        marginHorizontal: 5
    }
})