import { StyleSheet, FlatList, View, Pressable } from 'react-native'
import DisplayCommandHistory from '../components/DisplayCommandHistory'
import commandHistory from '../database/commandhistory';

const CommandHistory = () =>{

  const renderItem = ({ item }) => (
    <DisplayCommandHistory restaurant={item.restaurant} product={item.product} price = {item.price} date = {item.date}/>
   );

    return(
      <View style={styles.body}>
        <FlatList
          data={commandHistory}
          renderItem={renderItem}
          keyExtractor={item => item.id}
          horizontal={false}
        />
      </View>
    )
}

const styles = StyleSheet.create({
    body: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
    }
})

export default CommandHistory;