import { StyleSheet, View } from 'react-native';
import Restaurant from '../components/Restaurant';
import SearchButton from '../components/SearchButton';
import DATA from '../database/db'


const Menu = () =>{
    return(
      <View style={styles.body}>
        <SearchButton style={styles.searchButton}/>

        <View style={styles.listRestaurants}>

          <View style={styles.containerTop}>
                    <Restaurant 
                      dbName = {DATA.Gastro}
                      image = {require("../assets/image/gastro.jpg")} 
                      title = {"Gastro"}
                      place = {"Mahazo, Analamahitsy, Talatamaty, Ivato"}
                      colorscheme = {"#db0014"}
                    />
                    <Restaurant 
                      dbName = {DATA.Extra}
                      image = {require("../assets/image/extra.jpg")}
                      title = {"Extra"}
                      place = {"Mahazo, Analamahitsy, Talatamaty, Ivato"}
                      colorscheme = {"#2e2e86"}
                    />
              </View>
              
              <View style={styles.containerBot}>
                    <Restaurant 
                      dbName = {DATA.KFC}
                      image = {require("../assets/image/kfc.png")}
                      title = {"KFC"}
                      place = {"Ambodivona"}
                      colorscheme = {"#9e080c"}
                    />
                    <Restaurant 
                      dbName = {DATA.BurgerKing}
                      image = {require("../assets/image/burger.png")}
                      title = {"BurgerKing"}
                      place = {"Behoririka"}
                      colorscheme = {"#17518f"}
                    />
              </View>
            </View>

      </View>
    )
}

const styles = StyleSheet.create({
  searchButton: {
    padding: 10
  },
  listRestaurants: {
    flexDirection: "column",
    marginTop: "15%"
  },
  containerTop: {
      flexDirection: "row",
  },
  containerBot: {
      flexDirection: "row",
  },
})

export default Menu;