import { View, Text, Image, Button, StyleSheet, Alert } from 'react-native'
import commandHistory from '../database/commandhistory'

function addHistory(resto, product, price){
  var id = 4;
  const currentDate = () => {
    var D = new Date().getDate();
    var M = new Date().getMonth() + 1;
    var h = new Date().getHours();
    var m = new Date().getMinutes();

    D - 10 < 0 ? D = "0" + D : D = D
    M - 10 < 0 ? M = "0" + M : M = M
    h - 10 < 0 ? h = "0" + h : h = h
    m - 10 < 0 ? m = "0" + m : m = m

    const current = D + "/" + M + " à " + h + ":" + m;

    return current;
  }

  commandHistory.push({id: id, restaurant: resto, product: product, price: price, date: currentDate() });

  id = id + 1;

  Alert.alert("Produit commandé");

}

const MenuItems = (props) => {
  return (
    <View style={styles.item(props.colorscheme)}>
        <Image
        source={props.img}
        style={{
            height: 100,
            width: 100
        }}
        />
        <Text style={styles.product}>{props.product}</Text>
        <Text style={styles.price}>{props.price} Ariary</Text>
        <Button onPress={() => addHistory(props.resto, props.product, props.price)} width='20' title="Commander"/>
    </View>
  )
}

const styles = StyleSheet.create({
    item: (colorscheme) => { return {
        backgroundColor: colorscheme,
        padding: 60,
        marginVertical: 8,
        marginHorizontal: 16,
        borderRadius: 10
      }},
    
      price: {
        fontSize: 15,
        paddingRight: 10,
        color: "#FFFFFF"
      },
    
      product: {
        fontSize: 20,
        color: "#FFFFFF"
    
      }
})

export default MenuItems