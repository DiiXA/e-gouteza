import { Alert, Modal, StyleSheet, Text, Pressable, View } from "react-native";
import React, { useState } from "react";
import { Searchbar } from 'react-native-paper';

const SearchButton = () => {
  const [modalVisible, setModalVisible] = useState(false);
  return (
    <View style={styles.centeredView}>
        <Modal
            animationType="slide"
            transparent={true}
            visible={modalVisible}
            onRequestClose={() => {
            setModalVisible(!modalVisible);
            }}
        >
            <View style={styles.topView}>
                <View style={styles.modalView}>
                    <Searchbar style={styles.searchBar} placeholder = "Recherche"/>
                </View>
            </View>
        </Modal>
        <Pressable
            style={[styles.button, styles.buttonOpen]}
            onPress={() => setModalVisible(true)}
        >
            <Text style={styles.textStyle}>Rechercher</Text>
        </Pressable>
    </View>
  )
}

export default SearchButton

const styles = StyleSheet.create({
    centeredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "flex-end",
        marginTop: -30
      },
      modalView: {
        marginTop: "15%",
        alignItems: "center",
        elevation: 5
      },
      button: {
        borderRadius: 20,
        padding: 10,
        elevation: 2
      },
      buttonOpen: {
        height: 40,
        borderWidth: 1,
        backgroundColor: "white",
        elevation: 3,
        zIndex: 3
      },
      textStyle: {
        color: "black",
        textAlign: "center"
      },
      searchBar: {
        borderRadius: 25
      }
})