import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, Pressable } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createDrawerNavigator } from '@react-navigation/drawer';
import Menu from './screens/Menu';
import CommandHistory from './screens/CommandHistory';
import SalesHistory from './screens/SalesHistory';

const Drawer = createDrawerNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Drawer.Navigator>
        <Drawer.Screen 
          name = "Menu"
          component={Menu}
        />
        <Drawer.Screen 
          name = "Historique de commande"
          component={CommandHistory}
        /> 
        <Drawer.Screen 
          name = "Historique des ventes"
          component={SalesHistory}
        /> 
      </Drawer.Navigator>
    </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  Searchbar: {
    borderRadius: 25,
    marginHorizontal: 10,
    width: 200
  }
})
