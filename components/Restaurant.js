import { StyleSheet, TouchableOpacity, View, Image, Modal, Pressable, Text } from 'react-native'
import React, { useState } from 'react'
import RestaurantMenu from './RestaurantMenu'

const Restaurant = (props) => {
  const [modalVisible, setModalVisible] = useState(false);
  return (
    <View>
        <TouchableOpacity style={styles.Restaurant} onPress={() => setModalVisible(true)}>
            <Image
                source={props.image}
                style={{
                    height: 180,
                    width: 180
                }}
            />
        </TouchableOpacity>
        <Modal
            animationType="slide"
            transparent={false}
            visible={modalVisible}
            onRequestClose={() => {
            setModalVisible(!modalVisible);
            }}
        >
            <View style={styles.topView}>
                <View style={styles.modalView}>
                    <Pressable
                        style={[styles.button, styles.buttonClose]}
                        onPress={() => setModalVisible(!modalVisible)}
                        >
                        <Text style={styles.Return(props.colorscheme)}>←</Text>
                    </Pressable>
                    <RestaurantMenu dbName = {props.dbName} image = {props.image} title = {props.title} place = {props.place} colorscheme = {props.colorscheme}/>
                </View>
            </View>
        </Modal>
    </View>
  )
}

export default Restaurant

const styles = StyleSheet.create({
    Restaurant : {
        height:200,
        width:200
    },
    Return : (colorscheme) => { return {
        fontSize: 50,
        marginTop : -20,
        backgroundColor: colorscheme,
        color: "black"
    }}
})