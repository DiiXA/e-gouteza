import { StyleSheet, Text, View, Pressable } from 'react-native';
import RestaurantSales from '../components/RestaurantSales';
import salesHistory from '../database/saleshistory';

const SalesHistory = () =>{
    return(
      <View style={styles.listRestaurants}>
        <RestaurantSales 
          dbName = {salesHistory.Gastro}
          title = {"Gastro"}
          colorscheme = {"#db0014"}
        />
        <RestaurantSales 
          dbName = {salesHistory.Extra}
          title = {"Extra"}
          colorscheme = {"#2e2e86"}
        />
        <RestaurantSales 
          dbName = {salesHistory.KFC}
          title = {"KFC"}
          colorscheme = {"#9e080c"}
        />
        <RestaurantSales 
          dbName = {salesHistory.BurgerKing}
          title = {"BURGER KING"}
          colorscheme = {"#17518f"}
        />
      </View>
    )
}

const styles = StyleSheet.create({
    listRestaurants: {
      flexDirection: "column",
    }
})

export default SalesHistory;