import { View, Text, StyleSheet } from 'react-native'
import React from 'react'

const DisplayCommandHistory = (props) => {
  return (
    <View>
      <Text style={styles.Commands}>{props.restaurant} {props.product} {props.price} Ariary {props.date}</Text>
    </View>
  )
}

const styles = StyleSheet.create({
  Commands: {
    padding: 50,
    backgroundColor: "#dedede",
    margin: 5,
    borderRadius: 10
  }
})

export default DisplayCommandHistory