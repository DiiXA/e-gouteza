import { View, FlatList, StyleSheet, Text, StatusBar, Image, ScrollView} from 'react-native'
import React from 'react'
import MenuItems from './MenuItems'

const RestaurantMenu = (props) => {

  const renderItem = ({ item }) => (
   <MenuItems price={item.price} product={item.product} img={item.img} resto={item.resto} colorscheme = {props.colorscheme}/>
  );

  return (
    <View>
      <ScrollView>

        <Image
              source={props.image}
              style={{
                  height: 180,
                  width: '100%'
              }}
          />

        <Text style={styles.title}>{props.title}</Text>
        <Text style={styles.place}>Point de vente: {props.place}</Text>

    
  
        <FlatList
          data={props.dbName}
          renderItem={renderItem}
          keyExtractor={item => item.id}
          horizontal={true}
        />
    
      </ScrollView>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: StatusBar.currentHeight || 0,
  },

  title: {
    marginVertical: 8,
    marginHorizontal: 16,
    fontSize: 40,
    fontWeight: 'bold',
    color: 'black'
  },

  place: {
    marginVertical: 8,
    marginHorizontal: 16,
    fontSize: 20,
    color: 'black'
  }
});

export default RestaurantMenu