const DATA = { 
  Gastro : [
    {
      id: '1',
      img: require("../assets/image/pizzaGP.jpg"),
      product: 'Pizza',
      price: 20000,
      resto: 'Gastro'
    },
    {
      id: '2',
      img: require("../assets/image/kebabGP.png"),
      product: 'Sandwich Kebab',
      price: 12000,
      resto: 'Gastro'
    },
    {
      id: '3',
      img: require("../assets/image/friteGP.png"),
      product: 'Frite',
      price: 4000,
      resto: 'Gastro'
    }
  ],
  Extra : [
    {
      id: '1',
      img: require("../assets/image/tacosEP.jpg"),
      product: 'Tacos',
      price: 10000,
      resto: 'Extra'
    },
    {
      id: '2',
      img: require("../assets/image/pizzaEP.jpg"),
      product: 'Pizza feuilletée',
      price: 17000,
      resto: 'Extra'
    },
    {
      id: '3',
      img: require("../assets/image/rollsEP.jpg"),
      product: 'Rolls',
      price: 10000,
      resto: 'Extra'
    }
  ],
  KFC : [
    {
      id: '1',
      img: require("../assets/image/zingerKFC.jpg"),
      product: 'Zinger',
      price: 17000,
      resto: 'KFC'
    },
    {
      id: '2',
      img: require("../assets/image/wingsKFC.jpg"),
      product: 'Hot Wings',
      price: 19000,
      resto: 'KFC'
    },
    {
      id: '3',
      img: require("../assets/image/popnuggetsKFC.jpg"),
      product: 'Popcorn Nuggets',
      price: 8000,
      resto: 'KFC'
    }
  ],
  BurgerKing : [
    {
      id: '1',
      img: require("../assets/image/beefBK.jpg"),
      product: 'Triple Burger',
      price: 22000,
      resto: 'BurgerKing'
    },
    {
      id: '2',
      img: require("../assets/image/whopperBK.png"),
      product: 'Whopper',
      price: 17000,
      resto: 'BurgerKing'
    },
    {
      id: '3',
      img: require("../assets/image/chikenBK.png"),
      product: 'Chiken Sandwich',
      price: 16000,
      resto: 'BurgerKing'
    }
  ],
}

export default DATA;